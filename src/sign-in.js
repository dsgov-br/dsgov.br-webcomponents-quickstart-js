;(function () {
  let isLogged = false
  let userMenuVisible = false

  const signIn = document.getElementById('sign-in')
  const avatar = document.getElementById('avatar-dropdown-trigger')
  const caretUp = document.getElementById('caret-up')
  const caretDown = document.getElementById('caret-down')
  const userMenu = document.getElementById('user-menu')
  const userMenuItems = userMenu.querySelectorAll('br-item')
  const isLoggedMessage = document.getElementById('logged-message')

  signIn.addEventListener('click', () => {
    isLogged = !isLogged
    toggleLoggedElements()
  })

  avatar.addEventListener('click', () => {
    userMenuVisible = !userMenuVisible
    toggleUserMenu()
  })

  // Itera pelos br-items, exceto o último
  for (let i = 0; i < userMenuItems.length; i++) {
    const brItem = userMenuItems[i]

    // Adiciona um event listener a todos, exceto ao último
    if (i < userMenuItems.length - 1) {
      brItem.addEventListener('click', () => {
        const texto = brItem.textContent.trim()
        console.log(texto)
      })
    } else {
      // Último elemento - chama a função toggleSignIn
      brItem.addEventListener('click', () => {
        isLogged = !isLogged
        toggleLoggedElements()
      })
    }
  }

  toggleLoggedElements()

  function toggleLoggedElements() {
    isLoggedMessage.innerHTML = ''
    console.log(isLogged)
    if (isLogged) {
      signIn.style.display = ' none'
      avatar.parentNode.style.display = ' block'
      avatar.setAttribute('data-visible', true)
      avatar.setAttribute('aria-expanded', true)
      isLoggedMessage.innerHTML = '<p>Usuário <b>autenticado</b></p>'
    } else {
      signIn.style.display = ' block'
      avatar.parentNode.style.display = 'none'
      avatar.setAttribute('data-visible', false)
      avatar.setAttribute('aria-expanded', false)
      isLoggedMessage.innerHTML = '<p>Usuário <b>não autenticado</b></p>'
      userMenuVisible = false
      toggleUserMenu()
    }
  }

  function toggleUserMenu() {
    if (userMenuVisible) {
      caretUp.style.display = ' none'
      caretDown.style.display = ' block'
      userMenu.style.display = ' block'
    } else {
      caretUp.style.display = ' block'
      caretDown.style.display = ' none'
      userMenu.style.display = ' none'
    }
  }
})()
