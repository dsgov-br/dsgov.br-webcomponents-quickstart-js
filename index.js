async function getReadmeContent() {
  const readmeFile = './README.md'
  const content = await fetch(readmeFile).then((response) => response.text())
  return content
}

document.addEventListener('DOMContentLoaded', function () {
  getReadmeContent().then(function (file) {
    document.getElementById('readme-content').innerHTML = marked.parse(file)
    hljs.highlightAll()
  })
})
