# CHANGELOG

## [1.3.0](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.2.0...v1.3.0) (11/09/2023)

### ✨ NOVIDADES

- formulário, signin e cookiebar atualizados conforme os padrões ([fb69f1f](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/fb69f1f601b8be16a98edd0417ef816d8b45fa9a))

### 🪲 CORREÇÕES

- corrige o breadcrumb e imagens no gitpages ([24b9275](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/24b92759e5ce28860c2d31f2e40a2039239b5e48))

## [1.2.0](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.1.1...v1.2.0) (14/06/2023)

### 📚 Documentação

- atualiza url da wiki ([1528696](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/152869669bd46349288ef9226267a9a55634e76b))

### ✨ Novidades

- implementa exemplo de uso do componente cookiebar ([1018b47](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/1018b472b3eb8f68e70a2a684ee7953b21519cb7))
- quickstart para demostrar o uso dos wc em JavaScript puro ([ac43d8a](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/ac43d8aabefe660b6b1a81582f3489ef4c972c75))

## [1.1.1](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.1.0...v1.1.1) (02/01/2023)

### :bug: Correções

- texto sobre versão do wc usada ([ea327ca](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/ea327ca8c5204d451a47d27b9c2c2a35f174e2f8))

## [1.1.0](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.0.4...v1.1.0) (28/12/2022)

### :sparkles: Novidades

- atualiza web componentes para 1.4.1 ([571a2e8](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/571a2e82d3f2bb5e370cec50dea2b98f39b13220))

## [1.0.4](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.0.3...v1.0.4) (18/10/2022)

### :bug: Correções

- atualiza as dependências do projeto ([9a8f440](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/9a8f440d6b640fe256d73ecd3e7eb423602a0d20))
- remove tag menu obsoleta ([9cacb26](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/9cacb26e701e4a7e4c0f4f0a94184d3ccc4fcd73))

## [1.0.3](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.0.2...v1.0.3) (2022-09-27)

### Bug Fixes

- atualiza @govbr-ds/webcomponents para 1.3.1 ([1f6c37b](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/1f6c37bf8fc183bc290403f60c08aa5a1e134a92))

## [1.0.2](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.0.1...v1.0.2) (2022-09-19)

### Bug Fixes

- padronização da estrutura HTML ([c30f1f0](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/c30f1f04294c8a7eed8bc0844b315aad03c5b017))

## [1.0.1](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/compare/v1.0.0...v1.0.1) (2022-09-19)

### Bug Fixes

- **deps:** webcomponents atualizados para a 1.3.0 ([9759634](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/9759634bd6a85712278282756cb191ac2542ae38))

## 1.0.0 (2022-09-08)

### Features

- atualizado para versão 1.2.0 dos wc ([3738343](https://gitlab.com/govbr-ds/bibliotecas/wc/govbr-ds-wc-quickstart-js/commit/3738343d55772f39f85d71d855dc586a0cfba7f3))
